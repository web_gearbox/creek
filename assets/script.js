var that;

var entryDelay = (function(){
    var entryTimer = 0;
    return function(callback, ms){
        clearTimeout (entryTimer);
        entryTimer = setTimeout(callback, ms);
    };
})();

if(typeof Object.keys == 'undefined'){
    Object.keys = function(obj){
        return $.map(obj, function(element,index) {return index})
    }
}

creek = {
    elements:{},
    function_index:[],
    depshow:false,

    init:function(){

        this.elements.err = $('#error');
        this.elements.loading = $('#loading');
        this.elements.content = $('#content');
        this.elements.filterbox = $('#filterbox');
        this.noerror();
        this.elements.loading.html('Loading the index').show();

        that = this;

        $('#button').mousedown(function(event){
            $(this).toggleClass('pressed');
            $('#popup').toggle();
            event.stopPropagation();
        });

        $('#colorize').change(function(event){
            that.popularity = this.checked;
            that.showIndex();
            that.filterIndex();
        });

        $('#depshow').change(function(event){
            that.depshow = this.checked;
            that.showIndex();
            that.filterIndex();
        });

        $('#reverse').change(function(event){
            that.reverseSort = !this.checked;
            that.showIndex();
            that.filterIndex();
        });

        $('#popup input[name=sort]').change(function(event){
            if(this.checked && this.value)that.criteria=this.value;
            that.showIndex();
            that.filterIndex();
            console.log('Kinda.. Wow!');
        });

        $.getJSON('data.json',function(data){
            that.function_index = data.index;
            that.minCounter = data.minCounter;
            that.maxCounter = data.maxCounter;

            that.showIndex();
            that.elements.loading.hide();


            that.elements.filterbox.keyup(function(event){
                entryDelay(function(){
                    that.filterIndex();
                },50);
            });

        })
        .error(function(a,b,c){
                console.log(a);
                that.error('Error during request','Something bad happened.</p><p>Error text status was : '+ a.statusText + ' (' + c + ')');
                that.elements.loading.hide();
        });
    },
    filterIndex:function(){
        var input = that.elements.filterbox.val().toLowerCase();

        that.elements.itemlist.find('li').each(function(){
            var elem = $(this);
            var test = elem.attr('data-title').toLowerCase();

            if(input=='')elem.show();
            else if(test.indexOf(input)!=-1){
                elem.show();
            }
            else elem.hide();
        });

    },
    criteria:'title',
    reverseSort:true,
    showIndex:function() {

        this.elements.content.html('');
        this.elements.content.append($('<ul id="itemlist"></ul>'));
        this.elements.itemlist = $('#itemlist');

        function_index_sorted_keys = Object.keys(this.function_index);


        function_index_sorted_keys = function_index_sorted_keys.sort(function(a,b){
            return ((that.function_index[a][that.criteria]> that.function_index[b][that.criteria]) * 2 - 1) * (that.reverseSort*2-1);
        });


        for (var id in function_index_sorted_keys){


            var el = this.function_index[function_index_sorted_keys[id]];

            if(this.depshow || !el.deprecated) {

                var li = document.createElement('li');
                li.innerHTML = '<a href="http://wiki.secondlife.com/wiki/' + el.title + '">' + el.title + '</a>';
                if (el.deprecated)li.innerHTML += '<span class="deprecated"></span>';
                if (el.is_new)li.innerHTML += '<span class="new"></span>';
                li.setAttribute('data-id', el.id);
                li.setAttribute('data-title', el.title);

                if (this.popularity && el.counter) {
                    li.style.opacity = el.counter / this.maxCounter + 0.2;
                }
                li.onclick = this.elementClick;
                this.elements.itemlist.append(li);

            }
        }

    },
    elementClick : function(event){

        event.stopPropagation();
        event.preventDefault();

        var url = 'http://wiki.secondlife.com/wiki/'+$(this).attr('data-title');
        if(event.button == 1){
            console.log('Opening new tab at '+url);
            var win=window.open(url, '_blank');
            win.focus();
        }
        else{
            console.log('Following the link '+url);
            document.location.href = url;
        }

        return false;
    },
    noerror:function(){
        this.elements.err.hide();
    },
    error:function(title,text){
        this.elements.err.html('<h3>'+title+'</h3>'+'<p>'+text+'</p>').show();
    }
};

$(function(){
    creek.init();
});